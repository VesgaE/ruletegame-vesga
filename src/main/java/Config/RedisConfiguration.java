package Config;
import Models.Bet;
import Models.Rulete;
import Models.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedisConfiguration {
    @Bean
    JedisConnectionFactory jedisConnectionFactory(){

        return new JedisConnectionFactory();
    }
    @Bean
    RedisTemplate<String, User> userRedisTemplate(){
        final RedisTemplate<String, User> userRedisTemplate = new RedisTemplate<>();
        userRedisTemplate.setConnectionFactory(jedisConnectionFactory());

        return userRedisTemplate;
    }
    @Bean
    RedisTemplate<String, Bet> betRedisTemplate(){
        final RedisTemplate<String, Bet> betRedisTemplate = new RedisTemplate<>();
        betRedisTemplate.setConnectionFactory(jedisConnectionFactory());

        return betRedisTemplate;
    }
    @Bean
    RedisTemplate<String, Rulete> ruleteRedisTemplate(){
        final RedisTemplate<String, Rulete> ruleteRedisTemplate = new RedisTemplate<>();
        ruleteRedisTemplate.setConnectionFactory(jedisConnectionFactory());

        return ruleteRedisTemplate;
    }
}
