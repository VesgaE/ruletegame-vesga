package Controllers;
import Models.User;
import Repositories.RedisRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
@Controller
public class AppController {
    @Autowired
    private RedisRepositoryInterface redisRepository;
    @GetMapping("/users")
    public Map<String, User> findAllUsers(){
        return redisRepository.findAllUsers();
    }
    @GetMapping("/users/ID/{IDUser}")
    public User findUserByID(@PathVariable int IDUser){
        return redisRepository.findUserByUserID(IDUser);
    }
    @GetMapping("/users/name/{userName}")
    public User findUserByName(@PathVariable String userName){
        return redisRepository.findUserByUserName(userName);
    }
    @PostMapping("/users")
    public void createUser(User newUser){
        redisRepository.insertUser(newUser);
    }
    @DeleteMapping("/users/{IDUser}")
    public void deleteUser(@PathVariable int IDUser){
        redisRepository.deleteUser(IDUser);
    }

}
