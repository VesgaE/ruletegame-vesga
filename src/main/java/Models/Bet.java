package Models;
import InterfacesClasses.RuleteInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class Bet {
    private int IDBet;
    private User Gambler;
    private double StakeValue;
    private BetState State;
    private boolean isColorBet;
    @Autowired
    private RuleteInterface Rulete;
    public Bet(int IDBet, User Gambler, double StakeValue, boolean isColorBet) {
        this.IDBet = IDBet;
        this.Gambler = Gambler;
        this.StakeValue = StakeValue;
        this.State = BetState.UNKNOWN;
        this.isColorBet = isColorBet;
    }
    public int getIDBet() {

        return IDBet;
    }
    public void setIDBet(int IDBet) {
        this.IDBet = IDBet;
    }
    public User getGambler() {

        return Gambler;
    }
    public void setGambler(User gambler) {
        Gambler = gambler;
    }
    public double getStakeValue() {

        return StakeValue;
    }
    public void setStakeValue(double stakeValue) {
        StakeValue = stakeValue;
    }
    public BetState getState() {

        return State;
    }
    public void setState(BetState state) {
        State = state;
    }
    public boolean isColorBet() {
        return isColorBet;
    }
    public void setColorBet(boolean colorBet) {
        isColorBet = colorBet;
    }
    public RuleteInterface getRulete() {

        return Rulete;
    }
    public void setRulete(RuleteInterface rulete) {

        Rulete = rulete;
    }
}
