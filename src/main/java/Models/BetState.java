package Models;
public enum BetState {
    SUCCESS, DENIED, LOST, WON, UNKNOWN;
}
