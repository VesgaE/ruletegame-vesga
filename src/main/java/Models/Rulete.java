package Models;
import InterfacesClasses.RuleteInterface;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
@Component
public class Rulete implements RuleteInterface {
    private int IDRulete;
    private RuleteState RuleteStatus;
    private ArrayList<Bet> Bets;

    public Rulete(int IDRulete) {
        this.IDRulete = IDRulete;
        RuleteStatus = RuleteState.CLOSED;
        Bets = new ArrayList<>();
    }
    @Override
    public int SpinBall() {

        return (int) Math.floor(Math.random()*37);
    }
    public int getIDRulete() {

        return IDRulete;
    }
    public void setIDRulete(int IDRulete) {
        this.IDRulete = IDRulete;
    }
    public RuleteState getRuleteStatus() {

        return RuleteStatus;
    }
    public void setRuleteStatus(RuleteState ruleteStatus) {
        RuleteStatus = ruleteStatus;
    }
    public ArrayList<Bet> getBets() {

        return Bets;
    }
    public void setBets(ArrayList<Bet> bets) {
        Bets = bets;
    }
}
