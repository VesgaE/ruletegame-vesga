package Models;
import org.springframework.stereotype.Component;
@Component
public class User {
    private String UserName;
    private double Credits;
    private UserRole Role;
    private String Password;
    public User(String userName, double credits, UserRole role, String password) {
        UserName = userName;
        Credits = credits;
        Role = role;
        Password = password;
    }
    public String getUserName() {
        return UserName;
    }
    public void setUserName(String userName) {
        UserName = userName;
    }
    public double getCredits() {
        return Credits;
    }
    public void setCredits(double credits) {
        Credits = credits;
    }
    public UserRole getRole() {
        return Role;
    }
    public void setRole(UserRole role) {
        Role = role;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String password) {
        Password = password;
    }
}
