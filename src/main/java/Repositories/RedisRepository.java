package Repositories;
import Models.Bet;
import Models.Rulete;
import Models.User;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class RedisRepository implements RedisRepositoryInterface{
    private static final String USER_KEY = "User";
    private static final String BET_KEY = "Bet";
    private static final String RULETE_KEY = "Rulete";
    private RedisTemplate<String, User> redisUserTemplate;
    private RedisTemplate<String, Bet> redisBetTemplate;
    private RedisTemplate<String, Rulete> redisRuleteTemplate;
    private HashOperations hashOperationsUsers, hashOperationsBets, hashOperationsRuletes;
    public RedisRepository(RedisTemplate<String, User> redisUserTemplate, RedisTemplate<String, Bet> redisBetTemplate, RedisTemplate<String, Rulete> redisRuleteTemplate) {
        this.redisUserTemplate = redisUserTemplate;
        this.redisBetTemplate = redisBetTemplate;
        this.redisRuleteTemplate = redisRuleteTemplate;
    }
    @PostConstruct
    private void init(){
        hashOperationsUsers = redisUserTemplate.opsForHash();
        hashOperationsRuletes = redisRuleteTemplate.opsForHash();
        hashOperationsBets = redisBetTemplate.opsForHash();
    }
    @Override
    public Map<String, User> findAllUsers() {
        return hashOperationsUsers.entries(USER_KEY);
    }
    @Override
    public User findUserByUserID(int IDUser) {
        return (User) hashOperationsUsers.get(USER_KEY, String.valueOf(IDUser));
    }
    @Override
    public User findUserByUserName(String userName) {
        Map<String, User> allUsers = findAllUsers();
        for(Map.Entry<String, User> userIterator : findAllUsers().entrySet()){
            if(userIterator.getValue().getUserName().equals(userName)) return userIterator.getValue();
        }
        return null;
    }
    @Override
    public void insertUser(User newUser) {
        hashOperationsUsers.put(USER_KEY, UUID.randomUUID().toString(), newUser);
    }
    @Override
    public void deleteUser(int IDUser) {
        hashOperationsUsers.delete(USER_KEY, String.valueOf(IDUser));
    }
    @Override
    public Map<String, Rulete> findAllRuletes() {
        return null;
    }
    @Override
    public Rulete findRuleteByRuleteID(int IDRulete) {
        return null;
    }
    @Override
    public void insertRulete(Rulete newRulete) {
    }
    @Override
    public void deleteRulete(int IDRulete) {
    }
    @Override
    public Map<String, Bet> findAllBets() {
        return null;
    }
    @Override
    public Map<String, Bet> findBetsByRuleteID(int IDRulete) {
        return null;
    }
    @Override
    public Map<String, Bet> findBetsByUserID(int IDUser) {
        return null;
    }
    @Override
    public Bet findBetByUserIDAndRuleteID(int IDRulete, int IDUser) {
        return null;
    }
    @Override
    public Bet findBetByUserIDBet(int IDBet) {
        return null;
    }
    @Override
    public void insertBet(Bet newBet) {
    }
    @Override
    public void deleteBet(int IDBet) {
    }
}
