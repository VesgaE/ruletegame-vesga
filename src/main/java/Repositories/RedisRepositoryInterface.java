package Repositories;
import Models.Bet;
import Models.Rulete;
import Models.User;

import java.util.ArrayList;
import java.util.Map;
public interface RedisRepositoryInterface {
    Map<String, User> findAllUsers();
    User findUserByUserID(int IDUser);
    User findUserByUserName(String userName);
    void insertUser(User newUser);
    void deleteUser(int IDUser);
    Map<String, Rulete> findAllRuletes();
    Rulete findRuleteByRuleteID(int IDRulete);
    void insertRulete(Rulete newRulete);
    void deleteRulete(int IDRulete);
    Map<String, Bet> findAllBets();
    Map<String, Bet> findBetsByRuleteID(int IDRulete);
    Map<String, Bet> findBetsByUserID(int IDUser);
    Bet findBetByUserIDAndRuleteID(int IDRulete,int IDUser);
    Bet findBetByUserIDBet(int IDBet);
    void insertBet(Bet newBet);
    void deleteBet(int IDBet);
}
