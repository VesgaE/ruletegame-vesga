package Services;
import Models.User;
import Repositories.RedisRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
@Service
public class UserRedisService implements UserDetailsService {
    @Autowired
    private RedisRepositoryInterface RedisRepository;
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User userFound = RedisRepository.findUserByUserName(userName);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ADMIN"));
        authorities.add(new SimpleGrantedAuthority("DEFAULT"));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(userFound.getUserName(), userFound.getPassword(), authorities);
        return userDetails;
    }
}
