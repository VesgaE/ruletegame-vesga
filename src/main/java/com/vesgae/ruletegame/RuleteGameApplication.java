package com.vesgae.ruletegame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuleteGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuleteGameApplication.class, args);
    }

}
